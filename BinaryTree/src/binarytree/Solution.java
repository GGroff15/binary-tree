package binarytree;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Solution {

	
	public static void main(String[] args) {
		var binaryTree = new BinaryTree();

		var scanner = new Scanner(System.in);
		
		var input = scanner.nextLine();
		input = scanner.nextLine();
		addValues(input, binaryTree);
		
		scanner.close();
		
		binaryTree.transversePreOrder();
	}
	
	private static void addValues(String input, BinaryTree binaryTree) {
		String[] values = input.split(" ");
		Arrays.asList(values).stream().map(Integer::valueOf).forEach(binaryTree::add);
	}

}

class Node {

	private int value;
	private Node left;
	private Node right;
	
	public Node(final int value) {
		this.value = value;
		left = null;
		right = null;
	}

	public int getValue() {
		return value;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
	
}

class BinaryTree {
	
	private Node root;
	
	public void add(int value) {
		root = addRecursivaly(root, value);
	}
	
	private static Node addRecursivaly(Node current, int value) {
		if (Objects.isNull(current)) {
			return new Node(value);
		}
		
		if (value < current.getValue()) {
			current.setLeft(addRecursivaly(current.getLeft(), value));
		} 
		
		if(value > current.getValue()) {
			current.setRight(addRecursivaly(current.getRight(), value));			
		}
		
		return current;	
	}
	
	public void levelOrder() {
		if (Objects.isNull(root)) {
			return;
		}
		List<String> order = new LinkedList<>();
		
		Queue<Node> nodes = new LinkedList<>();
		nodes.add(root);
		
		while (!nodes.isEmpty()) {
			var node = nodes.remove();
			order.add(String.valueOf(node.getValue()));
			
			if (Objects.nonNull(node.getLeft())) {
				nodes.add(node.getLeft());
			}
			
			if (Objects.nonNull(node.getRight())) {
				nodes.add(node.getRight());
			}
		}
		
		System.out.print(order.stream().collect(Collectors.joining(" ")));
	}
	
	public void transversePreOrder() {
		List<String> order = new LinkedList<>();
		transversePreOrder(root, order);
		System.out.print(order.stream().collect(Collectors.joining(" ")));
	}
	
	private static void transversePreOrder(Node node, List<String> order) {
		if (Objects.nonNull(node)) {
			order.add(String.valueOf(node.getValue()));
			transversePreOrder(node.getLeft(), order);
			transversePreOrder(node.getRight(), order);
		}
	}

}